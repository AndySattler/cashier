﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Entities
{
    public class Account
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
