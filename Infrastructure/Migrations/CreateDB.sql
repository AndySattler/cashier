CREATE DATABASE [Cashier]
GO
USE [Cashier]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 3/19/2021 8:50:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](max) NOT NULL,
	[LastName] [varchar](max) NOT NULL,
 CONSTRAINT [PK_Account] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Transaction]    Script Date: 3/19/2021 8:50:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Transaction](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AccountId] [int] NOT NULL,
	[TransactionDate] [datetime] NOT NULL,
	[Amount] [decimal](19, 4) NOT NULL,
	[BalanceAfter] [decimal](19, 4) NOT NULL,
 CONSTRAINT [PK_Transaction] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Transaction]  WITH CHECK ADD  CONSTRAINT [FK_Transaction_Account] FOREIGN KEY([AccountId])
REFERENCES [dbo].[Account] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Transaction] CHECK CONSTRAINT [FK_Transaction_Account]
GO
/****** Object:  StoredProcedure [dbo].[addAccount]    Script Date: 3/19/2021 8:50:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[addAccount] @firstname varchar(max), @lastname varchar(max)
AS

insert into [Account] (FirstName, LastName)
values (@firstname, @lastname)

select * from Account where Id = SCOPE_IDENTITY()
GO
/****** Object:  StoredProcedure [dbo].[addTransaction]    Script Date: 3/19/2021 8:50:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[addTransaction] @accountid int, @amount decimal(19,4)
AS

declare @balance decimal(19,4) = 0
declare @transactionDate datetime

set @transactionDate = GETDATE()

select top (1) @balance = BalanceAfter from [Transaction] where AccountId = @accountid
order by TransactionDate desc

set @balance = @balance + @amount

insert into [Transaction] (AccountId, TransactionDate, Amount, BalanceAfter)
values (@accountid, @transactionDate, @amount, @balance)

select * from [Transaction] where id = SCOPE_IDENTITY()
GO
/****** Object:  StoredProcedure [dbo].[getAccount]    Script Date: 3/19/2021 8:50:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[getAccount] @id int 
AS

select * from Account where Id = @id

GO
/****** Object:  StoredProcedure [dbo].[getAccounts]    Script Date: 3/19/2021 8:50:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[getAccounts] 
AS

select * from Account

GO
/****** Object:  StoredProcedure [dbo].[getAccountsOverview]    Script Date: 3/19/2021 8:50:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[getAccountsOverview]
AS

select ISNULL(tr.BalanceAfter,0) [balance], a.id [accountId], a.FirstName, a.LastName
from account a
left outer join 
(
	[transaction] tr 
	join 
	(
		select max(transactionDate) lastTrans, AccountId
		from [Transaction]
		group by AccountId
	) lastTransaction 
	on tr.TransactionDate = lastTransaction.lastTrans
	and tr.AccountId = lastTransaction.AccountId
) on a.Id = tr.AccountId
GO
/****** Object:  StoredProcedure [dbo].[getTransactionsByAccount]    Script Date: 3/19/2021 8:50:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[getTransactionsByAccount] @accountId int
AS

select * from [Transaction] 
where AccountId = @accountId
order by TransactionDate desc
GO
USE [master]
GO
ALTER DATABASE [Cashier] SET  READ_WRITE 
GO
