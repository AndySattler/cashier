﻿using Microsoft.Extensions.DependencyInjection;
using Infrastructure.Interfaces;
using Infrastructure.Repositories;

namespace Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services)
        {
            services.AddTransient<ITransactionRepository, TransactionRepository>()
                    .AddTransient<IAccountRepository, AccountRepository>();

            return services;
        }
    }
}
