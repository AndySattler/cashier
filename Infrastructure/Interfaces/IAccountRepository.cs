﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Infrastructure.Dtos;

namespace Infrastructure.Interfaces
{
    public interface IAccountRepository
    {
        Task<IEnumerable<AccountDto>> GetAll();
        Task<AccountDto> Get(int id);
        Task<AccountDto> Add(string firstname, string lastname);
        Task<IEnumerable<AccountOverviewDto>> GetAccountsOverview();
    }
}
