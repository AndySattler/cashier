﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Infrastructure.Dtos;

namespace Infrastructure.Interfaces
{
    public interface ITransactionRepository
    {
        Task<IEnumerable<TransactionDto>> GetTransactionsByAccountId(int id);
        Task<TransactionDto> Add(int accountId, decimal amount);
    }
}
