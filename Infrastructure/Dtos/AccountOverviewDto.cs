﻿namespace Infrastructure.Dtos
{
    public class AccountOverviewDto    
    {
        public int AccountId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public decimal Balance { get; set; }
    }
}
