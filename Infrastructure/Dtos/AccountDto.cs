﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Dtos
{
    public class AccountDto
    {

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
