﻿using AutoMapper;
using Infrastructure.Entities;

namespace Infrastructure.Dtos
{
    public class AutoMapping : Profile
    {
        public AutoMapping()
        {
            CreateMap<AccountDto, Account>().ReverseMap();
            CreateMap<AccountOverviewDto, AccountOverview>().ReverseMap();
            CreateMap<TransactionDto, Transaction>().ReverseMap();
        }
    }
}
