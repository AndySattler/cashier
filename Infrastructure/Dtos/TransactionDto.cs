﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Dtos
{
    public class TransactionDto
    {
        public int Id { get; set; }
        public int AccountId { get; set; }
        public DateTime TransactionDate { get; set; }
        public decimal Amount { get; set; }
        public decimal BalanceAfter { get; set; }
    }
}
