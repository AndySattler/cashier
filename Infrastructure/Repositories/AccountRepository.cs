﻿using System;
using System.Collections.Generic;
using System.Text;
using Infrastructure.Interfaces;
using Infrastructure.Entities;
using Infrastructure.Dtos;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
using System.Linq;
using AutoMapper;

namespace Infrastructure.Repositories
{
    public class AccountRepository : IAccountRepository
    {
        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;

        public AccountRepository(IConfiguration configuration, IMapper mapper)
        {
            _configuration = configuration;
            _mapper = mapper;
        }

        public async Task<IEnumerable<AccountDto>> GetAll()
        {
            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                string sql = "exec getAccounts";
                var result = await connection.QueryAsync<Account>(sql);
                return _mapper.Map<IEnumerable<AccountDto>>(result);
            }
        }

        public async Task<AccountDto> Add(string firstname, string lastname)
        {
            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                string sql = "exec addAccount @firstname, @lastname";
                var values = new
                {
                    firstname = firstname,
                    lastname = lastname
                };
                var result = await connection.QuerySingleAsync<Account>(sql, values);
                return _mapper.Map<AccountDto>(result);
            }
        }

        public async Task<AccountDto> Get(int id)
        {
            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                string sql = "exec getAccount @id";
                var values = new { id = id };
                var result = await connection.QuerySingleAsync<Account>(sql, values);
                return _mapper.Map<AccountDto>(result);
            }
        }

        public async Task<IEnumerable<AccountOverviewDto>> GetAccountsOverview()
        {
            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                string sql = "exec getAccountsOverview";
                var result = await connection.QueryAsync<AccountOverview>(sql);
                return _mapper.Map<IEnumerable<AccountOverviewDto>>(result);
            }
        }
    }
}
