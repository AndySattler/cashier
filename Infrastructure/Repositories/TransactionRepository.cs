﻿using System;
using System.Collections.Generic;
using System.Text;
using Infrastructure.Interfaces;
using Infrastructure.Dtos;
using Infrastructure.Entities;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
using System.Linq;
using AutoMapper;

namespace Infrastructure.Repositories
{
    public class TransactionRepository : ITransactionRepository
    {
        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;

        public TransactionRepository(IConfiguration configuration, IMapper mapper)
        {
            _configuration = configuration;
            _mapper = mapper;
        }

        public async Task<TransactionDto> Add(int accountId, decimal amount)
        {
            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                string sql = "exec addTransaction @accountid, @amount";
                var values = new { 
                    accountid = accountId, 
                    amount = amount 
                };
                
                var result = await connection.QuerySingleAsync<Transaction>(sql, values);
                return _mapper.Map<TransactionDto>(result);
            }
        }     

        public async Task<IEnumerable<TransactionDto>> GetTransactionsByAccountId(int accountId)
        {
            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                string sql = "exec gettransactionsByAccount @id";
                var values = new { id = accountId };
                var result = await connection.QueryAsync<Transaction>(sql, values);
                return _mapper.Map<IEnumerable<TransactionDto>>(result);
            }
        }
    }
}
