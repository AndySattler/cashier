# An Example Cashier Application using SignalR

Run the CreateDB.sql script under Cashier\Infrastructure\Migrations to create the database.  The connection string in the appsettings.json file will also need to be updated to point to the database.