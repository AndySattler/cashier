﻿using Cashier.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Infrastructure.Interfaces;
using System.Threading.Tasks;
using AutoMapper;

namespace Cashier.Controllers
{
    public class AccountController : Controller
    {
        private readonly ILogger<AccountController> _logger;
        private readonly IAccountRepository _accountRepository;
        private readonly ITransactionRepository _transactionRepository;
        private readonly IMapper _mapper;

        public AccountController(ILogger<AccountController> logger, IAccountRepository accountRepository, ITransactionRepository transactionRepository, IMapper mapper)
        {
            _logger = logger;
            _accountRepository = accountRepository;
            _transactionRepository = transactionRepository;
            _mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            AccountViewPageModel model = new AccountViewPageModel()
            {
                Accounts = _mapper.Map<IEnumerable<AccountBalanceViewModel>>(await _accountRepository.GetAccountsOverview())
            };

            return View(model);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if(!id.HasValue)
                RedirectToAction("Index", "Account");

            AccountViewModel account = _mapper.Map<AccountViewModel>(await _accountRepository.Get(id.Value));
            IEnumerable<TransactionViewModel> transactions = _mapper.Map<IEnumerable<TransactionViewModel>>(await _transactionRepository.GetTransactionsByAccountId(id.Value));


            return View( new AccountDetailsPageViewModel() {
                AccountId = account.Id.Value,
                FirstName = account.FirstName,
                LastName = account.LastName,
                Transactions = transactions
            });
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [HttpPost]        
        public async Task<IActionResult> AddAccount([FromBody]AccountViewModel model)
        {
            if (model.FirstName.Trim() == "" || model.LastName.Trim() == "")
                throw new Exception("Invalid Data");


            var result = await _accountRepository.Add(model.FirstName, model.LastName);


            return Json(result.Id);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
