﻿"use strict";

var connection = new signalR.HubConnectionBuilder().withUrl("/accountHub").build();


$(document).ready(function () {  

    $('#transactionType').change(function () {
        if ($(this).val() === '') {
            $(this).addClass('is-invalid')
            $(this).removeClass('is-valid')
        }
        else {
            $(this).addClass('is-valid')
            $(this).removeClass('is-invalid')
        }

        WithdrawAmountCheck();
        handleSubmitButton();
    });

    $('#transactionAmount').keyup(function () {
        let value = $('#transactionAmount').val().trim();

        if (value === '' || isNaN(value) || HasMoreThan2Decimals(value)) {
            $(this).addClass('is-invalid')
            $(this).removeClass('is-valid')
        }
        else {
            $(this).addClass('is-valid')
            $(this).removeClass('is-invalid')

            WithdrawAmountCheck();
        }

        handleSubmitButton();
    });

    function WithdrawAmountCheck()
    {
        let value = $('#transactionAmount').val().trim();
        let balance = Number($('#balance').text().replace('$', '').replace(',', ''))

        if (value !== '' &&
            !isNaN(value) &&
            $('#transactionType').val() === 'debit' &&
            Number(value) > balance) {
            $('#submitError').text('You cannot withdraw more than is in the account')
        }
        else {
            $('#submitError').text('')
        }
    }

    function HasMoreThan2Decimals(amount) {
        return Number(amount) - Number(amount).toFixed(2)
    }

    function CanSubmit() {
        return $('#transactionAmount').hasClass('is-valid') &&
            $('#transactionType').hasClass('is-valid') &&
            $('#submitError').text() === ''
    }

    function handleSubmitButton() {
        if (CanSubmit()) {
            $('#btnSubmit').prop('disabled', false)            
        }
        else {
            $('#btnSubmit').prop('disabled', true)
        }
    }
});

function getAccountIdAsString() {
    let segments = location.href.split('/');
    return segments[segments.length - 1];
}

connection.on("AddTransaction", function (transaction) {
    $('#transactionAmount').val('');
    $('#transactionType').val('');

    $('#transactionAmount').keyup();
    $('#transactionType').change();

    const newRow = '<tr><td>' + transaction.id + '</td><td class="money">' + transaction.formattedAmount + '</td><td  class="money">' + transaction.formattedBalanceAfter + '</td><td>' + transaction.formattedDate + '</td></tr>';
    $(newRow).prependTo("table > tbody");

    $('#balance').text(transaction.formattedBalanceAfter);
    $('#myModal').modal('toggle');

});

connection.start().then(function () {
    connection.invoke("AddToGroup", getAccountId()).catch(function (err) {
        return console.error(err.toString());
    });
}).catch(function (err) {
    return console.error(err.toString());
});

document.getElementById("btnSubmit").addEventListener("click", function (event) {
    let amount = $('#transactionAmount').val();
    const transactionType = $('#transactionType').val();

    if (transactionType === 'debit' && amount > 0) {
        amount = 0 - amount;
    }

    connection.invoke("CreateTransaction", Number(amount), getAccountId()).catch(function (err) {
        return console.error(err.toString());
    });
    event.preventDefault();
});

function getAccountId() {
    let segments = location.href.split('/');
    return Number(segments[segments.length - 1]);
}

