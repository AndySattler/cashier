﻿$(document).ready(function () {
    $('#btnSubmit').click(function () {        
        if (CanSubmit()) {
            let firstName = $('#firstName').val()
            let lastName = $('#lastName').val()

            let data = { "firstName": firstName, "lastName": lastName }

            $.ajax({
                type: 'POST',
                data: JSON.stringify(data),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                url: '/Account/AddAccount',
                success: function (data) {
                    location.href = '/Account/Details/' + data 
                },
                error: function (data) {
                    alert('ERROR' + JSON.stringify(data))
                }
            });
        }
    });

    $('.required').keyup(function () {
        if ($(this).val() === '') {
            $(this).addClass('is-invalid')
            $(this).removeClass('is-valid')
        }
        else {
            $(this).addClass('is-valid')
            $(this).removeClass('is-invalid')
        }

        handleSubmitButton();        
    });

    function CanSubmit() {
        return $('#firstName').val() !== '' && $('#lastName').val() !== ''
    }

    function handleSubmitButton() {
        if (CanSubmit()) {
            $('#btnSubmit').prop('disabled', false)            
        }
        else {
            $('#btnSubmit').prop('disabled', true)
        }
    }
});


