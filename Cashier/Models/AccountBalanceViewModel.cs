using System;
using System.Collections.Generic;

namespace Cashier.Models
{
    public class AccountBalanceViewModel
    {
        public int AccountId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public decimal Balance { get; set; }
        public string FormattedBalance => string.Format("{0:C}", Balance);
        
    }
}
