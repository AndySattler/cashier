using System;

namespace Cashier.Models
{
    public class AccountViewModel
    {
        public int? Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
