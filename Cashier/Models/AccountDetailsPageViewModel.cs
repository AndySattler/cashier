using System;
using System.Collections.Generic;
using System.Linq;

namespace Cashier.Models
{
    public class AccountDetailsPageViewModel
    {
        public int AccountId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public IEnumerable<TransactionViewModel> Transactions { get; set; }
        public decimal CurrentBalance => Transactions.Count() == 0 ? 0 :  Transactions.OrderByDescending(p => p.TransactionDate).First().BalanceAfter;
        public string FormattedCurrentBalance => string.Format("{0:C}", CurrentBalance);
    }
}
