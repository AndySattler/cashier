﻿using AutoMapper;
using Infrastructure.Dtos;

namespace Cashier.Models
{
    public class Automapping : Profile
    {
        public Automapping()
        {
            CreateMap<AccountDto, AccountViewModel>();
            CreateMap<AccountOverviewDto, AccountBalanceViewModel>();
            CreateMap<TransactionDto, TransactionViewModel>();
        }
    }
}
