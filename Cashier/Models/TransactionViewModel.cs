using System;

namespace Cashier.Models
{
    public class TransactionViewModel
    {
        public int Id { get; set; }
        public int AccountId { get; set; }

        public DateTime TransactionDate { get; set; }
        public decimal Amount { get; set; }
        public decimal BalanceAfter { get; set; }
        public string FormattedAmount => string.Format("{0:C}", Amount);
        public string FormattedBalanceAfter => string.Format("{0:C}", BalanceAfter);
        public string FormattedDate => TransactionDate.ToString();
    }
}
