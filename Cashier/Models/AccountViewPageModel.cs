using System;
using System.Collections.Generic;

namespace Cashier.Models
{
    public class AccountViewPageModel
    {
        public IEnumerable<AccountBalanceViewModel> Accounts { get; set; }
    }
}
