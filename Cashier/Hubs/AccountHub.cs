﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using System.Linq;
using System;
using AutoMapper;
using Infrastructure.Interfaces;
using Cashier.Models;

namespace Cashier.Hubs
{
    public class AccountHub : Hub
    {
        private readonly IMapper _mapper;
        private readonly ITransactionRepository _transactionRepository;
        public AccountHub(IMapper mapper, ITransactionRepository transactionRepository)
        {
            _mapper = mapper;
            _transactionRepository = transactionRepository;
        }


       public async Task CreateTransaction(decimal amount, int accountId)
        {

            var result = await _transactionRepository.Add(accountId, amount);
            var transaction = _mapper.Map<TransactionViewModel>(result);

            await Clients.Group(accountId.ToString()).SendAsync("AddTransaction", transaction);            
        }

        public async Task AddToGroup(int accountId)
        {
            //Would want to do an authorization check here
            await Groups.AddToGroupAsync(Context.ConnectionId, accountId.ToString());
        }
    }
}
